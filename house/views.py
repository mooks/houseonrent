from django.shortcuts import render, redirect
from .models import user
from django.http import HttpResponse
from django.contrib import messages


# Create your views here.
def logout(request):
    # logout(request)
    return render(request,'index.html')
def index(request):
    return render(request,'index.html')

def signup(request):
    if request.method =='POST':
        name= request.POST.get('name')
        email= request.POST.get('email')
        psw= request.POST.get('pass')
        if user.objects.filter(email=email).exists():
            # messages.info(request,'User already exist')
            # return HttpResponse("<script>alert('User exists')</script>")
            return render(request,'signin.html')
            
        else:
            data=user()
            data.name= name
            data.email= email
            data.password= psw
            data.save()
            return render(request,'signin.html')
    else:    
        return render(request,'signup.html')

def signin(request):
    email=request.POST.get('name')
    if user.objects.filter(email=email).exists():
        psw=request.POST.get('pass')
        if user.objects.filter(password=psw).exists():
            return render(request,'index.html')
    else:
        return render(request,'signin.html')
