from django.urls import path
from .views import apiview

urlpatterns=[
    path('',apiview.as_view()),
]